package rpc

import (
	"context"
	"log"
	"net"
	"net/http"
	"net/rpc"
	"strconv"
	"sync"

	"gitlab.com/tharnadek/go-blockchain/pkg/blockchain"
)

// Node represents the communication layer of the blockchain
type Node struct {
	mu sync.Mutex // lock to prevent race condition access to the blockchain

	me    string
	peers map[string]struct{} // key is a string representation of net.IP

	bc    blockchain.Blockchain
	miner blockchain.Miner
}

// GetBlockchain is a request to get a range of blocks from our chain
func (n *Node) GetBlockchain(args *GetBlockchainArgs, reply *GetBlockchainReply) error {
	// TODO: GetBlockChain
	n.mu.Lock()
	defer n.mu.Unlock()

	reply.Blocks = n.bc.Blocks
	reply.Ok = true
	return nil
}

// sendGetBlocks requests the blockchain from index to the latest block from peer
// The returned blocks are validated and added to our chain
func (n *Node) sendGetBlocks(peer string) error {
	// Prepare args
	var args GetBlockchainArgs
	var reply GetBlockchainReply

	// Send the request
	client, err := rpc.Dial("tcp", peer)
	if err != nil {
		log.Printf("Failed to dial peer %s", peer)
		return err
	}
	err = client.Call("GetBlocks", args, reply)
	if err != nil {
		log.Panicf("RPC call to GetBlocks for peer %s failed: %v", peer, err)
	}

	n.mu.Lock()
	defer n.mu.Unlock()

	n.bc.Blocks = reply.Blocks

	return nil
}

// AppendBlock is a request to append a block to our chain
func (n *Node) AppendBlock(args *AppendBlockArgs, reply *AppendBlockReply) error {

	n.mu.Lock()

	// If the block is valid, we should add it
	if blockchain.ValidateBlock(args.Block, n.bc.LastBlock()) && n.bc.ValidateHashDifficulty(args.Block.Hash) {
		reply.Ok = true
		n.bc.AppendBlock(args.Block)
		n.mu.Unlock()
		return nil
	}

	// If the block is ahead of ours then we need to catch up
	if args.Block.Index > n.bc.LastBlock().Index+1 {
		n.mu.Unlock()
		err := n.sendGetBlocks(args.Sender)
		reply.Ok = (err == nil)
		return nil
	}

	// If the block is behind, we should return our index so that the node can catch up
	reply.Ok = false
	reply.Index = n.bc.LastBlock().Index
	n.mu.Unlock()
	return nil
}

// sendAppendBlock sends a block to a peer
func (n *Node) sendAppendBlock(block blockchain.Block, peer string) {
	// Prepare args
	args := AppendBlockArgs{
		Block:  block,
		Sender: n.me,
	}
	var reply AppendBlockReply

	// Send the request
	client, err := rpc.Dial("tcp", peer)
	if err != nil {
		log.Printf("Failed to dial peer %s", peer)
		return
	}
	defer client.Close()

	err = client.Call("AppendBlock", args, reply)
	if err != nil {
		log.Panicf("RPC call to AppendBlock for peer %s failed: %v", peer, err)
	}

	// Check if the block was accepted
	if !reply.Ok {
		log.Printf("Peer %s rejected our Block %d", peer, block.Index)
	}
}

// RegisterPeer returns a list of known peers and adds the requesting peer to the list if it doesn't exist
func (n *Node) RegisterPeer(args *RegisterPeerArgs, reply *RegisterPeerReply) error {
	n.mu.Lock()
	defer n.mu.Unlock()

	// Add the peer to our list (unless the peer is us)
	if !(n.me == args.IP) {
		n.peers[args.IP] = struct{}{}
	}

	// Return our peer list
	peers := make([]string, 0, len(n.peers))
	for k := range n.peers {
		peers = append(peers, k)
	}
	reply.Peers = peers
	return nil
}

// sendRegisterPeer sends a register peer request to notify them that we exist.
// Our peer list is updated to include the returned peers that are unknown to us,
// and new peers are contacted as well to propagate our presence to the network.
func (n *Node) sendRegisterPeer(destination string) {
	n.mu.Lock()
	defer n.mu.Unlock()

	// Prepare args
	args := RegisterPeerArgs{
		IP: n.me,
	}
	var reply RegisterPeerReply

	// Send the request
	// TODO: Should this be Dial or DialHTTP?
	client, err := rpc.Dial("tcp", destination)
	if err != nil {
		log.Panicf("Failed to dial peer %s", destination)
	}
	err = client.Call("RegisterPeer", args, reply)
	if err != nil {
		log.Panicf("RPC call to RegisterPeer for peer %s failed: %v", destination, err)
	}

	// Check for returned peers that we do not know yet
	for _, peer := range reply.Peers {
		if _, present := n.peers[peer]; !present {
			// Add this unknown peer to our peer list
			n.peers[peer] = struct{}{}

			// Notify them that we exist (and get their peer list)
			// The goroutine waits until our lock is released to run
			go n.sendRegisterPeer(peer)
		}
	}
}

// InitializeNode starts the given node with the provided parameters
// This function is not on the Node struct because we do not want to expose it
// to RPC calls
func InitializeNode(n *Node, port int, ip string, mine bool) {
	// If no network address is given, create a new chain
	if ip == "" {
		n.bc = blockchain.Blockchain{}
		n.bc.InitializeChain()
	}
	// If this node is required to mine, create a miner and start it
	if mine != false {
		n.miner = blockchain.Miner{
			Assign: make(chan blockchain.Block),
			Result: make(chan blockchain.Block),
			Chain:  n.bc,
		}
		go n.miner.Run(context.Background())
	}

	n.peers = make(map[string]struct{})
	// Register RPC Handler
	rpc.Register(n)
	rpc.HandleHTTP()
	l, e := net.Listen("tcp", ":"+strconv.Itoa(port))
	if e != nil {
		log.Fatal("listen error:", e)
	}
	n.me = "[::]:" + strconv.Itoa(port)
	log.Printf("listening on: %s", l.Addr().String())

	go http.Serve(l, nil)
}
