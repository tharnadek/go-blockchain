package rpc

import (
	"net/rpc"
	"sync"
	"testing"
	"time"

	"gitlab.com/tharnadek/go-blockchain/pkg/blockchain"
)

var (
	once             sync.Once
	portOne, portTwo int    = 1234, 5678
	addrOne, addrTwo string = "[::]:1234", "[::]:5678"
)

func startNodeOne() {
	var n = Node{}
	go InitializeNode(&n, portOne, "", false)
}

func startNodeTwo() {
	var n = Node{}
	InitializeNode(&n, portTwo, "", false)
}

func TestNode(t *testing.T) {
	once.Do(startNodeOne)
	time.Sleep(1 * time.Second)
	testOneNode(t, addrOne)
}

func testOneNode(t *testing.T, addr string) {
	client, err := rpc.DialHTTP("tcp", addr)
	if err != nil {
		t.Fatal("dialing", err)
	}
	defer client.Close()

	// Register a new peer (not a real node though)
	args := &RegisterPeerArgs{IP: addrTwo}
	reply := new(RegisterPeerReply)
	err = client.Call("Node.RegisterPeer", args, reply)
	if err != nil {
		t.Errorf("RegisterPeer: expected no error but got string %q", err.Error())
	}
	_, found := Find(reply.Peers, addrTwo)
	if !found {
		t.Errorf("Peer %s not find in reply to RegisterPeer", addrTwo)
	}

	// Get blocks (chain should just include genesis)
	getArgs := &GetBlockchainArgs{}
	getReply := new(GetBlockchainReply)
	err = client.Call("Node.GetBlockchain", getArgs, getReply)
	if err != nil {
		t.Errorf("GetBlockchain: expected no error but got string %q", err.Error())
	}
	if len(getReply.Blocks) != 1 {
		t.Errorf("GetBlockchain: expected 1 block but there were %d", len(getReply.Blocks))
	}

	// Create a new block
	nextBlock, err := blockchain.GenerateBlock(getReply.Blocks[0], "Test Block Content")
	// Get blocks (chain should just include genesis)
	appendArgs := &AppendBlockArgs{nextBlock, ""}
	appendReply := new(AppendBlockReply)
	err = client.Call("Node.AppendBlock", appendArgs, appendReply)
	if err != nil {
		t.Errorf("AppendBlock: expected no error but got string %q", err.Error())
	}
	if !appendReply.Ok {
		t.Errorf("AppendBlock: expected success but could not append block %d", len(getReply.Blocks))
	}

	// Get blocks (chain should just include genesis)
	getArgs = &GetBlockchainArgs{}
	getReply = new(GetBlockchainReply)
	err = client.Call("Node.GetBlockchain", getArgs, getReply)
	if err != nil {
		t.Errorf("GetBlockchain: expected no error but got string %q", err.Error())
	}
	if len(getReply.Blocks) != 2 {
		t.Errorf("GetBlockchain: expected 2 block but there were %d", len(getReply.Blocks))
	}

}

func Find(slice []string, val string) (int, bool) {
	for i, item := range slice {
		if item == val {
			return i, true
		}
	}
	return -1, false
}
