package rpc

import (
	"gitlab.com/tharnadek/go-blockchain/pkg/blockchain"
)

// RegisterPeerArgs rpc args
type RegisterPeerArgs struct {
	IP string
}

// RegisterPeerReply rpc reply
type RegisterPeerReply struct {
	Peers []string
}

// AppendBlockArgs rpc args
type AppendBlockArgs struct {
	Block  blockchain.Block
	Sender string
}

// AppendBlockReply rpc reply
type AppendBlockReply struct {
	Ok    bool
	Index int
}

// GetBlockchainArgs rpc args
type GetBlockchainArgs struct {
}

// GetBlockchainReply rpc reply
type GetBlockchainReply struct {
	Ok     bool
	Blocks []blockchain.Block
}
