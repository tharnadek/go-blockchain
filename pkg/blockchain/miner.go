package blockchain

import (
	"context"
	"log"
	"math/rand"
	"time"
)

// Miner performs a series of hashing tasks
type Miner struct {
	Assign chan Block
	Result chan Block
	Chain  Blockchain
}

// Run starts the miner, which accepts unsolved blocks via the Assign channel
// and returns solved blocks via the Result channel. Passing a new block via
// the Assign channel abandons any current block being solved. Runs until the
// provided context is cancelled.
func (m *Miner) Run(ctx context.Context) {
	childCtx, cancel := context.WithCancel(ctx)

	for {
		select {
		case block := <-m.Assign:
			// Stop mining if currently working on other block
			cancel()

			// Create new context for this block
			childCtx, cancel = context.WithCancel(ctx)

			// Mine it!
			go m.mine(childCtx, block)
		case <-ctx.Done():
			// Parent context cancelled, stop mining and return
			cancel()
			return
		}
	}
}

// randomly searches for a nonce for the provided block that satisfies
// the difficulty setting, running until a satisfactory nonce is found
// or the context is cancelled
func (m *Miner) mine(ctx context.Context, block Block) {
	for {
		block.Nonce = m.nonce(32)
		hash := CalculateHash(block)

		if _, ok := <-ctx.Done(); ok == false {
			// context is cancelled
			return
		}

		if m.Chain.ValidateHashDifficulty(hash) {
			log.Printf("Identified valid nonce for Block %d: %s", block.Index, block.Nonce)
			m.Result <- block
			return
		}
	}
}

var runes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

// returns a random nonce with length n
func (m *Miner) nonce(n int) string {
	rand.Seed(time.Now().UnixNano())
	b := make([]rune, n)
	for i := range b {
		b[i] = runes[rand.Intn(len(runes))]
	}
	return string(b)
}
