package blockchain

import (
	"testing"

	"github.com/davecgh/go-spew/spew"
)

func TestCreateGenesis(t *testing.T) {
	var b Blockchain
	b.InitializeChain()

	if len(b.Blocks) != 1 {
		t.Errorf("blockchain not initialized correctly")
	}
	spew.Dump(b.Blocks)
}

func TestGenerateBlock(t *testing.T) {
	var b Blockchain
	b.InitializeChain()
	testMessage := "Test Message"

	block, err := GenerateBlock(b.Blocks[len(b.Blocks)-1], testMessage)
	if err != nil {
		t.Errorf("generating block failed: %s", err)
	}
	if block.Message != testMessage {
		t.Errorf("block.Message != %s", testMessage)
		spew.Dump(block)
	}
}

func TestGenerateMultipleBlocks(t *testing.T) {
	var b Blockchain
	b.InitializeChain()
	msg := []string{"one", "two", "three", "four"}

	for _, msg := range msg {
		prevBlock := b.Blocks[len(b.Blocks)-1]
		nextBlock, err := GenerateBlock(b.Blocks[len(b.Blocks)-1], msg)
		if err != nil {
			t.Errorf("generating block failed: %s", err)
		}
		if nextBlock.Message != msg {
			t.Errorf("block.Message != %s", msg)
			spew.Dump(nextBlock)
		}
		if nextBlock.PrevHash != prevBlock.Hash {
			t.Errorf("block.Hash != prevBlock.Hash")
			spew.Dump(nextBlock)
		}
		b.Blocks = append(b.Blocks, nextBlock)
	}
	spew.Dump(b.Blocks)
}

func TestValidateBlockIncorrectIndices(t *testing.T) {
	prevBlock := Block{
		Index:     0,
		Timestamp: "2020-07-06",
		Message:   "Message",
		Hash:      "n0tar3alhas42351738a560889acf0b9a526b0ecce70a3117ea64d5341be5c66",
		PrevHash:  "",
	}
	block, _ := GenerateBlock(prevBlock, "Test_message")

	// block should be generated correctly
	if !ValidateBlock(block, prevBlock) {
		t.Errorf("ValidateBlock returned false for base case (sad!)")
	}
	// set index too low
	block.Index = 0
	if ValidateBlock(block, prevBlock) {
		t.Errorf("ValidateBlock returned true when index was too low")
	}
	// set index too high
	block.Index = 2
	if ValidateBlock(block, prevBlock) {
		t.Errorf("ValidateBlock returned true when index was too high")
	}
}
func TestValidateBlockIncorrectPrevHash(t *testing.T) {
	prevBlock := Block{
		Index:     0,
		Timestamp: "2020-07-06",
		Message:   "Message",
		Hash:      "n0tar3alhas42351738a560889acf0b9a526b0ecce70a3117ea64d5341be5c66",
		PrevHash:  "",
	}
	block, _ := GenerateBlock(prevBlock, "bad_block")
	// block should be generated correctly
	if !ValidateBlock(block, prevBlock) {
		t.Errorf("ValidateBlock returned false for base case (sad!)")
		spew.Dump(block)
	}

	// block should be generated correctly
	if !ValidateBlock(block, prevBlock) {
		t.Errorf("ValidateBlock returned false for base case (sad!)")
	}
	// set index too low
	block.Index = 0
	if ValidateBlock(block, prevBlock) {
		t.Errorf("ValidateBlock returned true when index was too low")
	}
}

func TestValidateBlockIncorrectHash(t *testing.T) {
	prevBlock := Block{
		Index:     0,
		Timestamp: "2020-07-06",
		Message:   "Message",
		Hash:      "n0tar3alhas42351738a560889acf0b9a526b0ecce70a3117ea64d5341be5c66",
		PrevHash:  "",
	}
	block, _ := GenerateBlock(prevBlock, "bad_block")
	// block should be generated correctly
	if !ValidateBlock(block, prevBlock) {
		t.Errorf("ValidateBlock returned false for base case (sad!)")
		spew.Dump(block)
	}

	// block should be generated correctly
	if !ValidateBlock(block, prevBlock) {
		t.Errorf("ValidateBlock returned false for base case (sad!)")
	}
	// set index too low
	block.Hash = "n0tar3alhas42351738a560889acf0b9a526b0ecce70a3117ea64d5341be5c66"
	if ValidateBlock(block, prevBlock) {
		t.Errorf("ValidateBlock returned true when index was too low")
	}
}
