package blockchain

import (
	"log"
	"math/big"

	"crypto/sha256"
	"encoding/hex"
	"errors"
	"time"
)

// Block is the basic element of the blockchain
type Block struct {
	Index     int    // index of block in total history
	Timestamp string // time this block was generated
	Message   string // message to store in this block
	Hash      string // hash of this block
	PrevHash  string // hash of prev block
	Nonce     string // nonce used to create a hash
}

// Blockchain implements a blockchain type
type Blockchain struct {
	Blocks     []Block
	Difficulty big.Int
}

// ReplaceChain is called when two chains exist; function keeps the longest chain
func (b *Blockchain) ReplaceChain(nextBlocks []Block) {
	if len(nextBlocks) > len(b.Blocks) {
		b.Blocks = nextBlocks
	}
}

// LastBlock returns the last block in the blockchain
func (b *Blockchain) LastBlock() Block {
	return b.Blocks[len(b.Blocks)-1]
}

// AppendBlock appends a new block
func (b *Blockchain) AppendBlock(nextBlock Block) error {
	if !ValidateBlock(nextBlock, b.LastBlock()) {
		return errors.New("invalid block")
	}
	if !b.ValidateHashDifficulty(nextBlock.Hash) {
		return errors.New("invalid hash")
	}
	b.Blocks = append(b.Blocks, nextBlock)
	return nil
}

// InitializeChain creates a genesis block and other init tasks
func (b *Blockchain) InitializeChain() {
	var genesis = Block{
		0,
		time.Now().String(),
		"",
		"",
		"",
		"",
	}
	b.Difficulty = big.Int{}
	// Base16 representation of 2^256 (maximum 256 bit value)
	b.Difficulty.SetString("10000000000000000000000000000000000000000000000000000000000000000", 16)
	b.Blocks = append(b.Blocks, genesis)
}

// ValidateHashDifficulty checks whether a hash meets the difficulty criteria
func (b *Blockchain) ValidateHashDifficulty(hash string) bool {
	hashBytes, err := hex.DecodeString(hash)
	if err != nil {
		log.Panicf("Failed to parse hash as valid hex string: %s", hash)
	}
	value := big.NewInt(0)
	value = value.SetBytes(hashBytes)

	return value.Cmp(&b.Difficulty) < 0
}

// CalculateHash given a block calculates a deterministic hash for it
func CalculateHash(block Block) string {
	record := string(block.Index) + block.Timestamp + block.Message + block.PrevHash
	h := sha256.New()
	h.Write([]byte(record))
	hashed := h.Sum(nil)
	return hex.EncodeToString(hashed)
}

// GenerateBlock given a previous block and a blockrate, calculates a new block
func GenerateBlock(lastBlock Block, Message string) (Block, error) {
	t := time.Now()
	var nextBlock = Block{
		Index:     lastBlock.Index + 1,
		Timestamp: t.String(),
		Message:   Message,
		PrevHash:  lastBlock.Hash,
	}
	nextBlock.Hash = CalculateHash(nextBlock)
	return nextBlock, nil
}

// ValidateBlock confirms that the block's hash is valid
func ValidateBlock(nextBlock Block, lastBlock Block) bool {
	if lastBlock.Index+1 != nextBlock.Index {
		return false
	}
	if lastBlock.Hash != nextBlock.PrevHash {
		return false
	}
	if CalculateHash(nextBlock) != nextBlock.Hash {
		return false
	}
	return true
}
