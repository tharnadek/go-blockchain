package main

import (
	"flag"

	"github.com/joho/godotenv"
	"gitlab.com/tharnadek/go-blockchain/pkg/rpc"

	"log"
)

func main() {
	// get environment vars here
	err := godotenv.Load()
	if err != nil {
		log.Print(err.Error())
	}

	// command line flags
	port := flag.Int("p", 1234, "the network port to use")
	ip := flag.String("n", "", "the address of an existing node on the network")
	mine := flag.Bool("m", false, "should the node also mine new blocks")
	difficulty := flag.Int("d", 0, "difficulty setting for the miner")

	flag.Parse()

	// log flags
	log.Print("Initializing Go-Blockchain . . .")
	log.Printf("VARS || port: %d, node ip: %s, mine: %t, difficulty: %d", *port, *ip, *mine, *difficulty)

	// create a new node and start it up
	var node = rpc.Node{}
	rpc.InitializeNode(&node, *port, *ip, *mine)
}
