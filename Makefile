all: test build
build: 
	go build ./cmd/go-blockchain
test: 
	go test ./...
clean: 
	rm -f go-blockchain
run:
	go run ./cmd/go-blockchain
